#ifndef FAKER_H
#define FAKER_H

#include <QtDBus/QDBusConnection>

#include "fakeproperty.h"
#include "fakepropertyadaptor.h"

class Faker : public QObject
{
    Q_OBJECT
public:
	explicit Faker(const QString& val, QObject *parent = 0);

signals:

public slots:
	void start();
	void stop();

protected:
	QDBusConnection _bus;
	FakeProperty *_property;
	FakePropertyAdaptor *_adaptor;
};

#endif // FAKER_H
