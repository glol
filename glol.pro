QT       += core dbus
QT       -= gui

TARGET = glol
CONFIG   += console
CONFIG   -= app_bundle

unix {
	CONFIG += link_pkgconfig
	PKGCONFIG += contextprovider-1.0 contextsubscriber-1.0
}

TEMPLATE = app

SOURCES += main.cpp \
    fakepropertyadaptor.cpp \
    fakeproperty.cpp \
    faker.cpp

HEADERS += \
    fakepropertyadaptor.h \
    fakeproperty.h \
    faker.h

OTHER_FILES += \
    com.javispedro.glol.context \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

unix:!symbian:!maemo5 {
    target.path = /usr/bin
    INSTALLS += target
    contextfiles.path += /usr/share/contextkit/providers
    contextfiles.files = com.javispedro.glol.context
    INSTALLS += contextfiles
}


