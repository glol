#include <stdio.h>
#include <signal.h>

#include <QtCore/QCoreApplication>
#include <QtCore/QStringList>

#include "faker.h"

static Faker* faker;

static void printHelp()
{
	printf("glol -- temporarily lock orientation in all applications\n");
	printf("Use: glol <p|portrait|l|landscape|ip|il>\n");
}

static void handleSignal(int signal)
{
	Q_UNUSED(signal);
	// Potentially unsafe. But...
	faker->stop();
	QCoreApplication::instance()->quit();
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	QMap<QString, QString> map;
	QString val;

	if (a.arguments().count() != 2) {
		printHelp();
		return 1;
	}

	map["p"] = "left";
	map["portrait"] = "left";
	map["l"] = "top";
	map["landscape"] = "top";
	map["ip"] = "right";
	map["il"] = "bottom";

	val = map[a.arguments().at(1).toLower()];
	if (val.isEmpty()) {
		printHelp();
		return 1;
	}

	signal(SIGINT, handleSignal);
	signal(SIGTERM, handleSignal);
	signal(SIGHUP, handleSignal);

	faker = new Faker(val);
	faker->start();

    return a.exec();
}
